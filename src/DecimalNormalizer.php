<?php

namespace Drupal\decimal;

use Brick\Math\BigDecimal;

/**
 * Decimal normalizer.
 */
final class DecimalNormalizer implements DecimalNormalizerInterface {

  /**
   * {@inheritdoc}
   */
  public function normalize(string $decimal, array $options = []): string | bool {
    $decimalCleaned = $this->cleanup($decimal);
    $bigDecimal = BigDecimal::of($decimalCleaned);
    return ($bigDecimal && ($bigDecimal instanceof BigDecimal)) ? $decimalCleaned : FALSE;
  }

  /**
   * Cleans up the string value for the coming decimal.
   *
   * @param string $decimal
   *   The default decimal as string.
   */
  private function cleanup(string $decimal): string {
    // Clean up different symbols.
    $replacements = [
      ' ' => '',
      ',' => '.',
      '+' => '',
      chr(0xC2) . chr(0xA0) => '',
    ];

    return str_replace(array_keys($replacements), array_values($replacements), $decimal);
  }

}
