<?php

namespace Drupal\decimal\Plugin\Field\FieldType;

use Drupal\Core\Field\FieldStorageDefinitionInterface;
use Drupal\Core\Field\Plugin\Field\FieldType\DecimalItem;
use Drupal\Core\Form\FormStateInterface;

/**
 * Defines the 'decimal' field type.
 *
 * @FieldType(
 *   id = "decimal_string",
 *   label = @Translation("Decimal (as string)"),
 *   description = @Translation("This field stores decimals in the database as a string."),
 *   category = @Translation("Number"),
 *   default_widget = "decimal_string",
 *   default_formatter = "decimal_string"
 * )
 */
class DecimalStringItem extends DecimalItem {

  /**
   * {@inheritdoc}
   */
  public static function schema(FieldStorageDefinitionInterface $field_definition) {
    return [
      'columns' => [
        'value' => [
          'type' => 'varchar',
          'length' => 32,
        ],
      ],
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function storageSettingsForm(array &$form, FormStateInterface $form_state, $has_data) {
    $element = parent::storageSettingsForm($form, $form_state, $has_data);

    $element['scale']['#max'] = 18;
    $element['precision']['#max'] = 32;

    return $element;
  }

  /**
   * {@inheritdoc}
   */
  public function preSave() {
    $decimal = \Drupal::service('decimal.normalizer')->normalize($this->value);
    $this->value = $decimal;
  }

}
