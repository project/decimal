<?php

namespace Drupal\decimal;

interface DecimalNormalizerInterface {

  /**
   * Normalizes a decimal.
   *
   * @param string $decimal The decimal.
   * @param array $options The parsing options.
   *
   * @return string|false The normalized decimal or FALSE on error.
   */
  public function normalize(string $decimal, array $options = []): string|bool;

}
