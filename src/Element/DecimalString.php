<?php

namespace Drupal\decimal\Element;

use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Render\Element;
use Drupal\Core\Render\Element\FormElementBase;

/**
 * Provides a decimal form element.
 *
 * Usage example:
 * @code
 * $form['decimal'] = [
 *   '#type' => 'decimal_string',
 *   '#title' => t('Decimal (string based)'),
 *   '#default_value' => '18.99',
 *   '#required' => TRUE,
 * ];
 * @endcode
 *
 * @FormElement("decimal_string")
 */
class DecimalString extends FormElementBase {

  /**
   * {@inheritdoc}
   */
  public function getInfo() {
    $class = get_class($this);
    return [
      '#min_fraction_digits' => 0,
      '#max_fraction_digits' => 18,
      '#min' => NULL,
      '#max' => NULL,

      '#size' => 32,
      '#maxlength' => 32,
      '#default_value' => NULL,
      '#element_validate' => [
        [$class, 'validateDecimal'],
      ],
      '#process' => [
        [$class, 'processElement'],
        [$class, 'processAjaxForm'],
        [$class, 'processGroup'],
      ],
      '#pre_render' => [
        [$class, 'preRenderNumber'],
        [$class, 'preRenderGroup'],
      ],
      '#input' => TRUE,
      '#theme' => 'input__textfield',
      '#theme_wrappers' => ['form_element'],
    ];
  }

  /**
   * {@inheritdoc}
   */
  public static function valueCallback(&$element, $input, FormStateInterface $form_state) {
    if ($input !== FALSE && $input !== NULL) {
      if (!is_scalar($input)) {
        $input = '';
      }
      return trim($input);
    }
    elseif (!empty($element['#default_value'])) {
      // Convert the stored number to the local format. For example, "9.99"
      // becomes "9,99" in many locales. This also strips any extra zeroes.
      $decimal_formatter = \Drupal::service('decimal.formatter');
      $decimal = (string) $element['#default_value'];
      $decimal = $decimal_formatter->format($decimal, [
        'use_grouping' => FALSE,
        'minimum_fraction_digits' => $element['#min_fraction_digits'],
        'maximum_fraction_digits' => $element['#max_fraction_digits'],
      ]);

      return $decimal;
    }

    return NULL;
  }

  /**
   * Builds the decimal_string form element.
   *
   * @param array $element
   *   The initial decimal_string form element.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   * @param array $complete_form
   *   The complete form structure.
   *
   * @return array
   *   The built decimal_string form element.
   */
  public static function processElement(array $element, FormStateInterface $form_state, array &$complete_form) {
    // Add a sensible default AJAX event.
    if (isset($element['#ajax']) && !isset($element['#ajax']['event'])) {
      $element['#ajax']['event'] = 'blur';
    }

    return $element;
  }

  /**
   * Validates the decimal_string element.
   *
   * Converts the number back to the standard format (e.g. "9,99" -> "9.99").
   *
   * @param array $element
   *   The form element.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   */
  public static function validateDecimal(array $element, FormStateInterface $form_state) {
    $value = trim($element['#value']);
    if ($value === '') {
      return;
    }
    $title = empty($element['#title']) ? $element['#parents'][0] : $element['#title'];
    $decimal_normalizer = \Drupal::service('decimal.normalizer');

    $value = $decimal_normalizer->normalize($value);

    // Validate value: should be decimal.
    if ($value === FALSE) {
      $form_state->setError($element, t('%title must be a decimal.', [
        '%title' => $title,
      ]));
      return;
    }

    // Validate min value: corresponding to min value.
    if (isset($element['#min']) && !is_null($element['#min']) && $value < $element['#min']) {
      $form_state->setError($element, t('%title must be higher than or equal to %min.', [
        '%title' => $title,
        '%min' => $element['#min'],
      ]));
      return;
    }

    // Validate max value: corresponding to max value.
    if (isset($element['#max']) && !is_null($element['#max']) && $value > $element['#max']) {
      $form_state->setError($element, t('%title must be lower than or equal to %max.', [
        '%title' => $title,
        '%max' => $element['#max'],
      ]));
      return;
    }

    $form_state->setValueForElement($element, $value);
  }

  /**
   * Prepares a #type 'decimal_string' render element for input.html.twig.
   *
   * @param array $element
   *   An associative array containing the properties of the element.
   *   Properties used: #title, #value, #description, #size, #maxlength,
   *   #placeholder, #required, #attributes.
   *
   * @return array
   *   The $element with prepared variables ready for input.html.twig.
   */
  public static function preRenderNumber(array $element) {
    $element['#attributes']['type'] = 'text';
    Element::setAttributes($element, ['id', 'name', 'value', 'size', 'maxlength', 'placeholder']);
    static::setAttributes($element, ['form-text']);

    return $element;
  }

}
