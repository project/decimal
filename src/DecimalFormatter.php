<?php

namespace Drupal\decimal;

use Brick\Math\BigDecimal;
use Brick\Math\RoundingMode;

/**
 * Decimal formatter.
 */
final class DecimalFormatter implements DecimalFormatterInterface {

  /**
   * {@inheritdoc}
   */
  public function format(string $decimal, array $options = []): string {
    if (!$decimal || $decimal === '') {
      $decimal = '0';
    }

    $bigDecimal = BigDecimal::of($decimal);
    if (isset($options['scale'])) {
      $bigDecimal = $bigDecimal->toScale($options['scale'], $options['rounding'] ?? RoundingMode::HALF_UP);
    }

    return (string) $bigDecimal;
  }

}
