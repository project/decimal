<?php

namespace Drupal\decimal;

interface DecimalFormatterInterface {

  /**
   * Formats a decimal.
   *
   * Supported options:
   * - scale: The scale. Default: 18.
   * - rounding: Different rounding modes.
   *
   * @param string $decimal The decimal.
   * @param array $options The formatting options.
   *
   * @return string The formatted decimal.
   */
  public function format(string $decimal, array $options = []): string;

}
